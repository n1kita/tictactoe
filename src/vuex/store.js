import axios from 'axios';
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);//Указываем VUE использоваться Vuex

//Ниже создаем хранилище, которое называет Store - глобальную БД
let store = new Vuex.Store({
    //state - состояние
    state: {
        products: []
    },
    //mutations - тут меняются данные state, являются синхронными
    mutations: {
        SET_PRODUCTS_TO_STATE: (state, products) => {
            state.products = products;
        }
    },
    //actions - действия, асинхронное
    actions: {
        GET_PRODUCTS_FROM_API({commit}) {
            return axios('http://localhost:3000/products', {
                method: "GET"
            }).then((products)=> {
                commit('SET_PRODUCTS_TO_STATE', products.data);
                return products;
            }).catch((error)=> {
                console.log(error)
                return error;
            })
        }
    },
    //getter -короткий путь для получения данных из state
    getters: {
        PRODUCTS(state) {
            return state.products;
        }
    }
});

export default store;//Экспортируем хранилище


// ЧТО ПРОИЗОШЛО:
// 1. Подключили Vuex
// 2. Попросили Vue исподьзовать Vuex
// 3. Создали Store (хранилище), в котором храним: 
//         state(состояние), 
//         mutations(мутации), 
//         actions(действия), 
//         getters(геттеры)
// 4. Создали пустое хранилище products: []
// 5. С помощью action GET_PRODUCTS_FROM_API вызываем GET запрос по URL(наш сервер)
// 6. После того как запрос выполнился без ошибок, вызываем мутацию SET_PRODUCTS_TO_STATE